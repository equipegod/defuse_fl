/*  -*- Last-Edit:  Mon Dec  7 10:31:51 1992 by Tarak S. Goradia; -*- */

extern void exit();
#include <stdio.h>
#define c_use(DATA, STRING1, STRING2)                                          \
  (fprintf(stdout, "use %s, %s\n", STRING1, STRING2), DATA)

#define p_use(DATA, STRING1) (fprintf(stdout, "p_use %s\n", STRING1), DATA)
// #define c_use(DATA, STRING1, STRING2)                                          \
//   ( DATA)
// #define p_use(DATA, STRING1) ( DATA)

void node(char n[]) {
   fprintf(stdout, "com %s\n", n);
  }

void def(char var[], char locus[]) {
  fprintf(stdout, "def %s %s\n", var, locus);
}

// int c_use(int data, char var[], char use_loc[]) {
//   fprintf(stdout, "use %s %s\n", var, use_loc);
//   return data;
// }

// int p_use(int data, char var[]) {
//   fprintf(stdout, "p_use %s\n", var);
//   return data;
// }

void p_loc(char begin_edge[], char end_edge[]) {
  fprintf(stdout, "loc %s-%s\n", begin_edge, end_edge);
}

int call(int data, char var[], char call_loc[]) {
  fprintf(stdout, "call %s %s\n", var, call_loc);
  return data;
}

void Caseerror();

typedef char bool;
#define false 0
#define true 1
#define NULL 0

#define MAXSTR 100
#define MAXPAT MAXSTR

#define ENDSTR '\0'
#define ESCAPE '@'
#define CLOSURE '*'
#define BOL '%'
#define EOL '$'
#define ANY '?'
#define CCL '['
#define CCLEND ']'
#define NEGATE '^'
#define NCCL '!'
#define LITCHAR 'c'
#define DITTO -1
#define DASH '-'

#define TAB 9
#define NEWLINE 10

#define CLOSIZE 1

typedef char character;
typedef char string[MAXSTR];

bool getline(s, maxsize) char *s;
int maxsize;
{
  node("getline.0");
  def("getline.s", "getline.0");
  def("getline.maxsize", "getline.0");

  node("getline.1");
  char *result;
  def("getline.result", "getline.1");

  node("getline.2");
  result = fgets(c_use(s, "getline.s", "getline.2"),
                 c_use(maxsize, "getline.maxsize", "getline.2"), stdin);
  def("getline.result", "getline.2");

  node("getline.3");
  return (c_use(result, "getline.result", "getline.3") != NULL);
}
int addstr(c, outset, j, maxset) char c;
char *outset;
int *j;
int maxset;
{
  node("addstr.0");
  def("addstr.c", "addstr.0");
  def("addstr.outset", "addstr.0");
  def("addstr.j", "addstr.0");
  def("addstr.maxset", "addstr.0");

  node("addstr.1");
  bool result;
  def("addstr.result", "addstr.1");

  node("addstr.2");
  if (p_use(*j, "addstr.j") >= p_use(maxset, "addstr.maxset")) {
    p_loc("addstr.2", "addstr.3");
    node("addstr.3");
    result = false;
    def("addr.result", "addstr.3");
  } else {
    p_loc("addstr.2", "addstr.4");
    node("addstr.4");
    outset[c_use(*j, "addstr.j", "addstr.4")] =
        c_use(c, "addstr.c", "addstr.4");
    def("addstr.outset", "addstr.4");
    node("addstr.5");
    *j = c_use(*j, "addstr.j", "addstr.5") + 1;
    def("addstr.j", "addstr.5");
    node("addstr.6");
    result = true;
    def("addstr.result", "addstr.6");
  }
  node("addstr.7");
  return c_use(result, "addstr.result", "addstr.7");
}

char esc(s, i) char *s;
int *i;
{
  node("esc.0");
  def("esc.s", "esc.0");
  def("esc.i", "esc.0");

  node("esc.1");
  char result;
  def("esc.result", "esc.1");
  node("esc.2");
  if (p_use(s[c_use(*i, "esc.i", "esc.2")], "esc.s") !=
      p_use(ESCAPE, "ESCAPE")) {
    p_loc("esc.2", "esc.3");
    node("esc.3");
    result = c_use(s[c_use(*i, "esc.i", "esc.3")], "esc.s", "esc.3");
    def("esc.result", "esc.3");
  } else {
    p_loc("esc.2", "esc.4");
    node("esc.4");

    if (p_use(s[c_use(*i, "esc.i", "esc.4") + 1], "esc.s") ==
        p_use(ENDSTR, "ENDSTR")) {
      p_loc("esc.4", "esc.5");
      node("esc.5");
      result = c_use(ESCAPE, "ESCAPE", "esc.5");
      def("esc.result", "esc.5");
    } else {
      p_loc("esc.4", "esc.6");
      node("esc.6");
      *i = c_use(*i, "esc.i", "esc.6") + 1;
      def("esc.i", "esc.6");
      node("esc.7");

      if (p_use(s[c_use(*i, "esc.i", "esc.7")], "esc.s") == 'n') {
        p_loc("esc.7", "esc.8");
        node("esc.8");

        result = c_use(NEWLINE, "NEWLINE", "esc.8");
        def("esc.result", "esc.8");
      } else {
        p_loc("esc.7", "esc.9");
        node("esc.9");

        if (p_use(s[c_use(*i, "esc.i", "esc.9")], "esc.s") == 't') {
          p_loc("esc.9", "esc.10");
          node("esc.10");
          result = c_use(TAB, "TAB", "esc.10");
          def("esc.result", "esc.10");
        } else {
          p_loc("esc.9", "esc.11");
          node("esc.11");

          result = c_use(s[c_use(*i, "esc.i", "esc.11")], "esc.s", "esc.11");
          def("esc.result", "esc.11");
        }
      }
    }
  }
  node("esc.12");
  return c_use(result, "esc.result", "esc.12");
}

void change();

void dodash(delim, src, i, dest, j, maxset) char delim;
char *src;
int *i;
char *dest;
int *j;
int maxset;
{
  node("dodash.0");
  def("dodash.delim", "dodash.0");
  def("dodash.src", "dodash.0");
  def("dodash.i", "dodash.0");
  def("dodash.dest", "dodash.0");
  def("dodash.j", "dodash.0");
  def("dodash.maxset", "dodash.0");

  node("dodash.1");
  int k;
  def("dodash.k", "dodash.1");

  node("dodash.2");
  bool junk;
  def("dodash.junk", "dodash.2");

  node("dodash.3");
  char escjunk;
  def("dodash.escjunk", "dodash.3");

  node("dodash.4");
  while ((p_use(src[c_use(*i, "dodash.i", "dodash.4")], "dodash.src") !=
          c_use(delim, "dodash.delim", "dodash.4")) &&
         (p_use(src[c_use(*i, "dodash.i", "dodash.4")], "dodash.src") !=
          c_use(ENDSTR, "ENDSTR", "dodash.4"))) {
    p_loc("dodash.4", "dodash.5");
    node("dodash.5");
    if (p_use(src[c_use(*i, "dodash.i", "dodash.5") - 1], "dodash.src") ==
        p_use(ESCAPE, "ESCAPE")) {
      p_loc("dodash.5", "dodash.6");
      node("dodash.6");
      escjunk = esc(c_use(src, "dodash.src", "dodash.6"),
                    c_use(i, "dodash.i", "dodash.6"));
      def("dodash.escjunk", "dodash.6");
      node("dodash.7");
      junk = addstr(c_use(escjunk, "dodash.escjunk", "dodash.7"),
                    c_use(dest, "dodash.dest", "dodash.7"),
                    c_use(j, "dodash.j", "dodash.7"),
                    c_use(maxset, "dodash.maxset", "dodash.7"));
      def("dodash.junk", "dodash.7");

    } else {
      p_loc("dodash.5", "dodash.8");

      node("dodash.8");

      if (p_use(src[c_use(*i, "dodash.i", "dodash.8")], "dodash.src") !=
          p_use(DASH, "DASH")) {
        p_loc("dodash.8", "dodash.9");

        node("dodash.9");

        junk = addstr(c_use(src[c_use(*i, "dodash.i", "dodash.9")],
                            "dodash.src", "dodash.9"),
                      c_use(dest, "dodash.dest", "dodash.9"),
                      c_use(j, "dodash.j", "dodash.9"),
                      c_use(maxset, "dodash.maxset", "dodash.9"));
        def("dodash.junk", "dodash.9");

      } else {
        p_loc("dodash.8", "dodash.10");
        node("dodash.10");

        if (p_use(*j, "dodash.j") <= 1 ||
            p_use(src[c_use(*i, "dodash.i", "dodash.10") + 1], "dodash.src") ==
                p_use(ENDSTR, "ENDSTR")) {
          p_loc("dodash.10", "dodash.11");
          node("dodash.11");

          junk = addstr(c_use(DASH, "DASH", "dodash.11"),
                        c_use(dest, "dodash.dest", "dodash.11"),
                        c_use(j, "dodash.j", "dodash.11"),
                        c_use(maxset, "dodash.maxset", "dodash.11"));
          def("dodash.junk", "dodash.11");
        } else {
          p_loc("dodash.10", "dodash.12");

          node("dodash.12");

          if ((isalnum(p_use(src[c_use(*i, "dodash.i", "dodash.12") - 1],
                             "dodash.src"))) &&
              (isalnum(p_use(src[c_use(*i, "dodash.i", "dodash.12") + 1],
                             "dodash.src"))) &&
              (p_use(src[c_use(*i, "dodash.i", "dodash.12") - 1],
                     "dodash.src") <=
               p_use(src[c_use(*i, "dodash.i", "dodash.12") + 1],
                     "dodash.src"))) {
            p_loc("dodash.12", "dodash.13");
            node("dodash.13");

            for (k = c_use(src[c_use(*i, "dodash.i", "dodash.13") - 1],
                           "dodash.src", "dodash.13") +
                     1,
                def("dodash.k", "dodash.13");
                 p_use(k, "dodash.13") <=
                 p_use(src[c_use(*i, "dodash.i", "dodash.13") + 1],
                       "dodash.src");
                 k = c_use(k, "dodash.k", "dodash.13") + 1,
                def("dodash.k", "dodash.13")) {
              p_loc("dodash.13", "dodash.14");
              node("dodash.14");

              junk = addstr(c_use(k, "dodash.k", "dodash.14"),
                            c_use(dest, "dodash.dest", "dodash.14"),
                            c_use(j, "dodash.j", "dodash.14"),
                            c_use(maxset, "dodash.maxset", "dodash.14"));
              def("dodash.junk", "dodash.14");

              node("dodash.13");
            }
            p_loc("dodash.13", "dodash.15");

            node("dodash.15");

            *i = c_use(*i, "dodash.i", "dodash.15") + 1;
            def("dodash.i", "dodash.15");
          } else {
            p_loc("dodash.12", "dodash.16");
            node("dodash.16");

            junk = addstr(c_use(DASH, "DASH", "dodash.16"),
                          c_use(dest, "dodash.dest", "dodash.16"),
                          c_use(j, "dodash.j", "dodash.16"),
                          c_use(maxset, "dodash.maxset", "dodash.16"));
            def("dodash.junk", "dodash.16");
          }
          node("dodash.17");


        }
      }
    }
    (*i) = c_use((*i), "dodash.i", "dodash.17") + 1; // verify this line
    def("dodash.i", "dodash.17");
    node("dodash.4");
  }
  p_loc("dodash.4", "dodash.18");
  node("dodash.18");
}

bool getccl(arg, i, pat, j) char *arg;
int *i;
char *pat;
int *j;
{
  node("getccl.0");
  def("getccl.arg", "getccl.0");
  def("getccl.i", "getccl.0");
  def("getccl.pat", "getccl.0");
  def("getccl.j", "getccl.0");

  node("getccl.1");
  int jstart;
  def("getccl.jstart", "getccl.1");

  node("getccl.2");
  bool junk;
  def("getccl.junk", "getccl.2");
  def("getccl.i", "getccl.0");

  node("getccl.3");
  *i = c_use(*i, "getccl.i", "getccl.3") + 1;
  def("getccl.i", "getccl.3");

  node("getccl.4");
  if (p_use(arg[c_use(*i, "getccl.i", "getccl.3")], "getccl.arg") ==
      p_use(NEGATE, "NEGATE")) {
    p_loc("getccl.4", "getccl.5");
    node("getccl.5");

    junk = addstr(
        c_use(NCCL, "NCCL", "getccl.5"), c_use(pat, "getccl.pat", "getccl.5"),
        c_use(j, "getccl.j", "getccl.5"), c_use(MAXPAT, "MAXPAT", "getccl.5"));
    def("getccl.junk", "getccl.5");
    node("getccl.6");

    *i = c_use(*i, "getccl.i", "getccl.6") + 1;
    def("getccl.i", "getccl.6");

  } else {
    p_loc("getccl.4", "getccl.7");
    node("getccl.7");

    junk = addstr(
        c_use(CCL, "CCL", "getccl.7"), c_use(pat, "getccl.pat", "getccl.7"),
        c_use(j, "getccl.j", "getccl.7"), c_use(MAXPAT, "MAXPAT", "getccl.7"));
    def("getccl.junk", "getccl.7");
  }
  node("getccl.8");

  jstart = c_use(*j, "getccl.j", "getccl.8");
  def("getccl.jstart", "getccl.8");
  node("getccl.9");

  junk = addstr(0, c_use(pat, "getccl.pat", "getccl.9"),
                c_use(j, "getccl.j", "getccl.9"),
                c_use(MAXPAT, "MAXPAT", "getccl.9"));
  def("getccl.junk", "getccl.9");
  node("getccl.10");

  dodash(
      c_use(CCLEND, "CCLEND", "getccl.10"),
      c_use(arg, "getccl.arg", "getccl.10"), c_use(i, "getccl.i", "getccl.10"),
      c_use(pat, "getccl.pat", "getccl.10"), c_use(j, "getccl.j", "getccl.10"),
      c_use(MAXPAT, "MAXPAT", "getccl.10"));
  node("getccl.11");

  pat[c_use(jstart, "getccl.jstart", "getccl.11")] =
      c_use(*j, "getccl.j", "getccl.11") -
      c_use(jstart, "getccl.jstart", "getccl.11") - 1;
  def("getccl.pat", "getccl.11");
  node("getccl.12");

  return (c_use(arg[c_use(*i, "getccl.i", "getccl.12")], "getccl.arg",
                "getccl.12") == c_use(CCLEND, "CCLEND", "getccl.11"));
}

void stclose(pat, j, lastj) char *pat;
int *j;
int lastj;
{
  node("stclose.0");
  def("stclose.pat", "stclose.0");
  def("stclose.j", "stclose.0");
  def("stclose.lastj", "stclose.0");

  node("stclose.1");
  int jt;
  def("stclose.jt", "stclose.1");

  node("stclose.2");
  int jp;
  def("stclose.jp", "stclose.2");

  node("stclose.3");
  bool junk;
  def("stclose.junk", "stclose.3");

  node("stclose.4");
  for (jp = c_use(*j, "stclose.j", "stclose.4") - 1,
      def("stclose.jp", "stclose.4");
       p_use(jp, "stclose.jp") >= p_use(lastj, "stclose.lastj");
       jp = c_use(jp, "stclose.jp", "stclose.4") - 1,
      def("stclose.jp", "stclose.4")) {
    p_loc("stclose.4", "stclose.5");
    node("stclose.5");
    jt = c_use(jp, "stclose.jp", "stclose.5") +
         c_use(CLOSIZE, "CLOSIZE", "stclose.5");
    def("stclose.jt", "stclose.5");
    node("stclose.6");
    junk = addstr(c_use(pat[c_use(jp, "stclose.jp", "stclose.6")],
                        "stclose.pat", "stclose.6"),
                  c_use(pat, "stclose.pat", "stclose.6"),
                  c_use(&jt, "stclose.jt", "stclose.6"),
                  c_use(MAXPAT, "stclose.MAXPAT", "stclose.6"));
    def("stclose.junk", "stclose.6");
    node("stclose.4");
  }
  p_loc("stclose.4", "stclose.7");
  node("stclose.7");
  *j = c_use(*j, "stclose.j", "stclose.7") +
       c_use(CLOSIZE, "CLOSIZE", "stclose.7");
  def("stclose.j", "stclose.7");

  node("stclose.8");
  pat[c_use(lastj, "stclose.lastj", "stclose.8")] =
      c_use(CLOSURE, "CLOSURE", "stclose.8");
  def("stclose.pat", "stclose.8");
}

bool in_set_2(c) char c;
{
  node("in_set_2.0");
  def("in_set_2.c", "in_set_2.0");
  node("in_set_2.1");
  return (
      c_use(c, "in_set_2.c", "in_set_2.1") == c_use(BOL, "BOL", "in_set_2.1") ||
      c_use(c, "in_set_2.c", "in_set_2.1") == c_use(EOL, "EOL", "in_set_2.1") ||
      c_use(c, "in_set_2.c", "in_set_2.1") ==
          c_use(CLOSURE, "CLOSURE", "in_set_2.1"));
}

bool in_pat_set(c) char c;
{
  node("in_pat_set.0");
  def("in_pat_set.c", "in_pat_set.0");
  node("in_pat_set.1");
  return (c_use(c, "in_pat_set.c", "in_pat_set.1") ==
              c_use(LITCHAR, "LITCHAR", "in_pat_set.1") ||
          c_use(c, "in_pat_set.c", "in_pat_set.1") ==
              c_use(BOL, "BOL", "in_pat_set.1") ||
          c_use(c, "in_pat_set.c", "in_pat_set.1") ==
              c_use(EOL, "EOL", "in_pat_set.1") ||
          c_use(c, "in_pat_set.c", "in_pat_set.1") ==
              c_use(ANY, "ANY", "in_pat_set.1") ||
          c_use(c, "in_pat_set.c", "in_pat_set.1") ==
              c_use(CCL, "CCL", "in_pat_set.1") ||
          c_use(c, "in_pat_set.c", "in_pat_set.1") ==
              c_use(NCCL, "NCCL", "in_pat_set.1") ||
          c_use(c, "in_pat_set.c", "in_pat_set.1") ==
              c_use(CLOSURE, "CLOSURE", "in_pat_set.1"));
}

int makepat(arg, start, delim, pat) char *arg;
int start;
char delim;
char *pat;
{
  node("makepat.0");
  def("makepat.arg", "makepat.0");
  def("makepat.start", "makepat.0");
  def("makepat.delim", "makepat.0");
  def("makepat.pat", "makepat.0");

  node("makepat.1");
  int result;
  def("makepat.result", "makepat.1");
  node("makepat.2");
  int i;
  def("makepat.i", "makepat.2");
  node("makepat.3");
  int j;
  def("makepat.j", "makepat.3");

  node("makepat.4");
  int lastj;
  def("makepat.lastj", "makepat.4");

  node("makepat.5");
  int lj;
  def("makepat.lj", "makepat.5");

  node("makepat.6");
  bool done;
  def("makepat.done", "makepat.6");

  node("makepat.7");
  bool junk;
  def("makepat.junk", "makepat.7");

  node("makepat.8");
  bool getres;
  def("makepat.getres", "makepat.8");

  node("makepat.9");
  char escjunk;
  def("makepat.escjunk", "makepat.9");

  node("makepat.10");
  j = 0;
  def("makepat.j", "makepat.10");

  node("makepat.11");
  i = c_use(start, "makepat.start", "makepat.11");
  def("makepat.i", "makepat.11");

  node("makepat.12");
  lastj = 0;
  def("makepat.lastj", "makepat.12");

  node("makepat.13");
  done = false;
  def("makepat.done", "makepat.13");

  node("makepat.14");
  while ((!p_use(done, "makepat.done")) &&
         (p_use(arg[c_use(i, "makepat.i", "makepat.14")], "makepat.arg") !=
          p_use(delim, "makepat.delim")) &&
         (p_use(arg[c_use(i, "makepat.i", "makepat.14")], "makepat.arg") !=
          p_use(ENDSTR, "ENDSTR"))) {
    p_loc("makepat.14", "makepat.15");
    node("makepat.15");
    lj = c_use(j, "makepat.j", "makepat.15");
    def("makepat.lj", "makepat.15");

    node("makepat.16");
    if ((p_use(arg[c_use(i, "makepat.i", "makepat.16")], "makepat.arg") ==
         p_use(ANY, "ANY"))) {
      p_loc("makepat.16", "makepat.17");
      node("makepat.17");

      junk = addstr(c_use(ANY, "ANY", "makepat.17"),
                    c_use(pat, "makepat.pat", "makepat.17"),
                    c_use(&j, "makepat,j", "makepat.17"),
                    c_use(MAXPAT, "MAXPAT", "makepat.17"));
      def("makepat.junk", "makepat.17");
    } else {
      p_loc("makepat.16", "makepat.18");
      node("makepat.18");

      if ((p_use(arg[c_use(i, "makepat.i", "makepat.18")], "makepat.arg") ==
           p_use(BOL, "BOL")) &&
          (p_use(i, "makepat.i") == p_use(start, "makepat.start"))) {
        p_loc("makepat.18", "makepat.19");
        node("makepat.19");

        junk = addstr(c_use(BOL, "BOL", "makepat.19"),
                      c_use(pat, "makepat.pat", "makepat.19"),
                      c_use(&j, "makepat.j", "makepat.19"),
                      c_use(MAXPAT, "MAXPAT", "makepat.19"));
        def("makepat.junk", "makepat.19");
      } else {
        p_loc("makepat.18", "makepat.20");
        node("makepat.20");

        if ((p_use(arg[c_use(i, "makepat.i", "makepat.20")], "makepat.arg") ==
             p_use(EOL, "EOL")) &&
            (p_use(arg[c_use(i, "makepat.i", "makepat.20") + 1],
                   "makepat.arg") == p_use(delim, "makepat.delim"))) {
          p_loc("makepat.20", "makepat.21");
          node("makepat.21");

          junk = addstr(c_use(EOL, "EOL", "makepat.21"),
                        c_use(pat, "makepat.pat", "makepat.21"),
                        c_use(&j, "makepat.j", "makepat.21"),
                        c_use(MAXPAT, "MAXPAT", "makepat.21"));
          def("makepat.junk", "makepat.21");
        } else {
          p_loc("makepat.20", "makepat.22");
          node("makepat.22");
          p_loc("makepat.20", "makepat.22");
          if ((p_use(arg[c_use(i, "makepat.i", "makepat.20")], "makepat.arg") ==
               p_use(CCL, "CCL"))) {
            p_loc("makepat.22", "makepat.23");
            node("makepat.23");
            getres = getccl(c_use(arg, "makepat.arg", "makepat.23"),
                            c_use(&i, "makepat.i", "makepat.23"),
                            c_use(pat, "makepat.pat", "makepat.23"),
                            c_use(&j, "makepat.j", "makepat.23"));
            def("makepat.getres", "makepat.23");

            node("makepat.24");
            done =
                (bool)(c_use(getres, "makepat.getres", "makepat.24") == false);
            def("makepat.done", "makepat.24");
          } else {
            p_loc("makepat.22", "makepat.25");
            node("makepat.25");
            if ((p_use(arg[c_use(i, "makepat.i", "makepat.25")],
                       "makepat.arg") == p_use(CLOSURE, "CLOSURE")) &&
                (p_use(i, "makepat.i") > p_use(start, "makepat.start"))) {
              p_loc("makepat.25", "makepat.26");
              node("makepat.26");
              lj = c_use(lastj, "makepat.lastj", "makepat.26");
              def("makepat.lj", "makepat.26");
              node("makepat.27");
              if (in_set_2(p_use(pat[c_use(lj, "makepat.lj", "makepat.27")],
                                 "makepat.27"))) {
                p_loc("makepat.27", "makepat.28");
                node("makepat.28");
                done = true;
                def("makepat.done", "makepat.28");
              } else {
                p_loc("makepat.27", "makepat.29");
                node("makepat.29");
                stclose(c_use(pat, "makepat.pat", "makepat.29"),
                        c_use(&j, "makepat.j", "makepat.29"),
                        c_use(lastj, "makepat.lastj", "makepat.29"));
              }
            } else {
              p_loc("makepat.25", "makepat.30");
              node("makepat.30");
              junk = addstr(c_use(LITCHAR, "LITCHAR", "makepat.30"),
                            c_use(pat, "makepat.pat", "makepat.30"),
                            c_use(&j, "makepat.j", "makepat.30"),
                            c_use(MAXPAT, "MAXPAT", "makepat.30"));
              def("makepat.junk", "makepat.30");

              node("makepat.31");
              escjunk = esc(c_use(arg, "makepat.arg", "makepat.31"),
                            c_use(&i, "makepat.i", "makepat.31"));
              def("makepat.escjunk", "makepat.31");

              node("makepat.32");
              junk = addstr(c_use(escjunk, "makepat.escjunk", "makepat.32"),
                            c_use(pat, "makepat.pat", "makepat.32"),
                            c_use(&j, "makepat.j", "makepat.32"),
                            c_use(MAXPAT, "MAXPAT", "makepat.32"));
              def("makepat.junk", "makepat.32");
            }
          }
        }
      }
    }
    node("makepat.33");
    lastj = c_use(lj, "makepat.lj", "makepat.33");
    def("makepat.lastj", "makepat.33");

    node("makepat.34");
    if ((!p_use(done, "makepat.done"))) {
      p_loc("makepat.34", "makepat.35");
      node("makepat.35");
      i = c_use(i, "makepat.i", "makepat.35") + 1;
      def("makepat.i", "makepat.35");
    } else {
      p_loc("makepat.34", "makepat.36");
      node("makepat.36");
    }
    node("makepat.14");
  }
  p_loc("makepat.14", "makepat.37");
  node("makepat.37");
  junk = addstr(c_use(ENDSTR, "ENDSTR", "makepat.37"),
                c_use(pat, "makepat.pat", "makepat.37"),
                c_use(&j, "makepat.j", "makepat.37"),
                c_use(MAXPAT, "MAXPAT", "makepat.37"));
  def("makepat.junk", "makepat.37");

  node("makepat.38");
  if ((p_use(done, "makepat.done")) ||
      (p_use(arg[c_use(i, "makepat.i", "makepat.25")], "makepat.arg") !=
       p_use(delim, "makepat.delim"))) {
    p_loc("makepat.38", "makepat.39");
    node("makepat.39");
    result = 0;
    def("makepat.result", "makepat.39");
  } else {
    p_loc("makepat.38", "makepat.40");
    node("makepat.40");
    if ((!p_use(junk, "makepat.junk"))) {
      p_loc("makepat.40", "makepat.41");
      node("makepat.41");
      result = 0;
      def("makepat.result", "makepat.41");
    } else {
      p_loc("makepat.40", "makepat.42");
      node("makepat.42");
      result = c_use(i, "makepat.i", "makepat.42");
      def("makepat.result", "makepat.42");
    }
  }
  node("makepat.43");
  return c_use(result, "makepat.result", "makepat.43");
}

int getpat(arg, pat) char *arg;
char *pat;
{
  node("getpat.0");
  def("getpat.arg", "getpat.0");
  def("getpat.pat", "getpat.0");

  node("getpat.1");
  int makeres;
  def("getpat.makeres", "getpat.1");

  node("getpat.2");
  makeres = makepat(c_use(arg, "getpat.arg", "getpat.2"), 0,
                    c_use(ENDSTR, "ENDSTR", "getpat.2"),
                    c_use(pat, "getpat.pat", "getpat.2"));
  def("getpat.makeres", "getpat.2");

  node("getpat.3");
  return (c_use(makeres, "getpat.makeres", "getpat.3") > 0);
}

int makesub(arg, from, delim, sub) char *arg;
int from;
character delim;
char *sub;
{
  node("makesub.0");
  def("makesub.arg", "makesub.0");
  def("makesub.from", "makesub.0");
  def("makesub.delim", "makesub.0");
  def("makesub.sub", "makesub.0");
  node("makesub.1");
  int result;
  def("makesub.result", "makesub.1");

  node("makesub.2");
  int i;
  def("makesub.i", "makesub.2");

  node("makesub.3");
  int j;
  def("makesub.j", "makesub.3");

  node("makesub.4");
  bool junk;
  def("makesub.junk", "makesub.4");

  node("makesub.5");
  character escjunk;
  def("makesub.escjunk", "makesub.5");

  node("makesub.6");
  j = 0;
  def("makesub.j", "makesub.6");

  node("makesub.7");
  i = c_use(from, "makesub.from", "makesub.7");
  def("makesub.i", "makesub.7");

  node("makesub.8");
  while ((p_use(arg[c_use(i, "makesub.i", "makesub.8")], "makesub.arg") !=
          p_use(delim, "makesub.delim")) &&
         (p_use(arg[c_use(i, "makesub.i", "makesub.8")], "makesub.arg") !=
          p_use(ENDSTR, "ENDSTR"))) {
    p_loc("makesub.8", "makesub.9");
    node("makesub.9");
    if ((p_use(arg[c_use(i, "makesub.i", "makesub.8")], "makesub.arg") ==
         (unsigned)('&'))) {
      p_loc("makesub.9", "makesub.10");
      node("makesub.10");
      junk = addstr(c_use(DITTO, "DITTO", "makesub.10"),
                    c_use(sub, "makesub.sub", "makesub.10"),
                    c_use(&j, "makesub.j", "makesub.10"),
                    c_use(MAXPAT, "MAXPAT", "makesub.10"));
      def("makesub.junk", "makesub.10");
    } else {
      p_loc("makesub.9", "makesub.11");
      node("makesub.11");
      escjunk = esc(c_use(arg, "makesub.arg", "makesub.11"),
                    c_use(&i, "makesub.i", "makesub.11"));
      def("makesub.escjunk", "makesub.11");
      node("makesub.12");
      junk = addstr(c_use(escjunk, "makesub.escjunk", "makesub.12"),
                    c_use(sub, "makesub.sub", "makesub.12"),
                    c_use(&j, "makesub.j", "makesub.12"),
                    c_use(MAXPAT, "MAXPAT", "makesub.12"));
      def("makesub.junk", "makesub.12");
    }

    node("makesub.13");
    i = c_use(i, "makesub.i", "makesub.13") + 1;
    def("makesub.i", "makesub.13");

    node("makesub.8");
  }
  p_loc("makesub.8", "makesub.14");
  node("makesub.14");
  if (p_use(arg[c_use(i, "makesub.i", "makesub.14")], "makesub.arg") !=
      p_use(delim, "makesub.delim")) {
    p_loc("makesub.14", "makesub.15");
    node("makesub.15");
    result = 0;
    def("makesub.result", "makesub.15");
  } else {
    p_loc("makesub.14", "makesub.16");
    node("makesub.16");
    junk = addstr(c_use(ENDSTR, "ENDSTR", "makesub.16"),
                  c_use(&(*sub), "makesub.sub", "makesub.16"),
                  c_use(&j, "makesub.j", "makesub.16"),
                  c_use(MAXPAT, "MAXPAT", "makesub.16"));
    def("makesub.junk", "makesub.16");
    node("makesub.17");
    if ((!p_use(junk, "makesub.junk"))) {
      p_loc("makesub.17", "makesub.18");
      node("makesub.18");
      result = 0;
      def("makesub.result", "makesub.18");

    } else {
      p_loc("makesub.17", "makesub.19");
      node("makesub.19");
      result = c_use(i, "makesub.i", "makesub.19");
      def("makesub.result", "makesub.19");
    }
  }
  node("makesub.20");
  return c_use(result, "makesub.result", "makesub.20");
}

bool getsub(arg, sub) char *arg;
char *sub;
{
  node("getsub.0");
  def("getsub.arg", "getsub.0");
  def("getsub.sub", "getsub.0");

  node("getsub.1");
  int makeres;
  def("getsub.makeres", "getsub.1");

  node("getsub.2");
  makeres = makesub(c_use(arg, "getsub.arg", "getsub.2"), 0,
                    c_use(ENDSTR, "ENDSTR", "getsub.2"),
                    c_use(sub, "getsub.sub", "getsub.2"));
  def("getsub.makeres", "getsub.2");

  node("getsub.3");
  return (c_use(makeres, "getsub.makeres", "getsub.3") > 0);
}

void subline();

bool locate(c, pat, offset) character c;
char *pat;
int offset;
{
  node("locate.0");
  def("locate.c", "locate.0");
  def("locate.pat", "locate.0");
  def("locate.offset", "locate.0");

  node("locate.1");
  int i;
  def("locate.i", "locate.1");

  node("locate.2");
  bool flag;
  def("locate.flag", "locate.2");

  node("locate.3");
  flag = false;
  def("locate.flag", "locate.3");

  node("locate.4");
  i = c_use(offset, "locate.offset", "locate.4") +
      c_use(pat[c_use(offset, "locate.offset", "locate.4")], "locate.pat",
            "locate.4");
  def("locate.i", "locate.4");

  node("locate.5");
  while ((p_use(i, "locate.i") > p_use(offset, "locate.offset"))) {
    p_loc("locate.5", "locate.6");
    node("locate.6");
    if (p_use(c, "locate.c") ==
        p_use(pat[c_use(i, "locate.i", "locate.6")], "locate.pat")) {
      p_loc("locate.6", "locate.7");
      node("locate.7");

      flag = true;
      def("locate.flag", "locate.7");
      node("locate.8");
      i = c_use(offset, "locate.offset", "locate.8");
      def("locate.i", "locate.8");
    } else {
      p_loc("locate.6", "locate.9");
      node("locate.9");
      i = c_use(i, "locate.i", "locate.9") - 1;
      def("locate.i", "locate.9");
    }
    node("locate.5");
  }
  p_loc("locate.5", "locate.10");
  node("locate.10");
  return c_use(flag, "locate.flag", "locate.10");
}

bool omatch(lin, i, pat, j) char *lin;
int *i;
char *pat;
int j;
{
  node("omatch.0");
  def("omatch.lin", "omatch.0");
  def("omatch.i", "omatch.0");
  def("omatch.pat", "omatch.0");
  def("omatch.j", "omatch.0");

  node("omatch.1");
  char advance;
  def("omatch.advance", "omatch.1");

  node("omatch.2");
  bool result;
  def("omatch.result", "omatch.2");

  node("omatch.3");
  advance = -1;
  def("omatch.advance", "omatch.3");

  node("omatch.4");
  if ((p_use(lin[c_use(*i, "omatch.i", "omatch.4")], "omatch.lin") ==
       p_use(ENDSTR, "ENDSTR"))) {
    p_loc("omatch.4", "omatch.5");
    node("omatch.5");
    result = false;
    def("omatch.result", "omatch.5");
  } else {
    p_loc("omatch.4", "omatch.6");
    node("omatch.6");
    if (!in_pat_set(
            p_use(pat[c_use(j, "omatch.j", "omatch6")], "omatch.pat"))) {
      p_loc("omatch.6", "omatch.8");
      node("omatch.8");
      // (void)fprintf(stdout, "in omatch: can't happen\n");

      node("omatch.9");
      abort();
    } else {
      p_loc("omatch.6", "omatch.10");
      node("omatch.10");
      switch (p_use(pat[c_use(j, "omatch.j", "omatch.10")], "omatch.pat")) {
      case LITCHAR:
        p_use(LITCHAR, "LITCHAR");
        p_loc("omatch.10", "omatch.11");
        node("omatch.11");
        if (p_use(lin[c_use(*i, "omatch.i", "omatch.11")], "omatch.lin") ==
            p_use(pat[c_use(j, "omatch.j", "omatch.11") + 1], "omatch.pat")) {
          p_loc("omatch.11", "omatch.12");
          node("omatch.12");
          advance = 1;
          def("omatch.advance", "omatch.12");
        } else {
          p_loc("omatch.11", "omatch.13");
          node("omatch.13");
        }
        node("omatch.14");
        break;
      case BOL:
        p_use(BOL, "BOL");
        p_loc("omatch.10", "omatch.15");
        node("omatch.15");
        if (p_use(*i, "omatch.i") == 0) {
          p_loc("omatch.15", "omatch.16");
          node("omatch.16");
          advance = 0;
          def("omatch.advance", "omatch.16");
        } else {
          p_loc("omatch.15", "omatch.17");

          node("omatch.17");
        }
        node("omatch.18");
        break;
      case ANY:
        p_use(ANY, "ANY");
        p_loc("omatch.10", "omatch.19");
        node("omatch.19");
        if (p_use(lin[c_use(*i, "omatch.i", "omatch.19")], "omatch.lin") !=
            p_use(NEWLINE, "NEWLINE")) {
          p_loc("omatch.19", "omatch.20");
          node("omatch.20");
          advance = 1;
          def("omatch.advance", "omatch.20");
        } else {
          p_loc("omatch.19", "omatch.21");
          node("omatch.21");
        }
        node("omatch.22");
        break;
      case EOL:
        p_use(EOL, "EOL");
        p_loc("omatch.10", "omatch.23");
        node("omatch.23");
        if (p_use(lin[c_use(*i, "omatch.i", "omatch.23")], "omatch.lin") ==
            p_use(NEWLINE, "NEWLINE")) {
          p_loc("omatch.23", "omatch.24");
          node("omatch.24");
          advance = 0;
          def("omatch.advance", "omatch.24");
        } else {
          p_loc("omatch.23", "omatch.25");
          node("omatch.25");
        }
        node("omatch.26");
        break;
      case CCL:
        p_use(CCL, "CCL");
        p_loc("omatch.10", "omatch.27");
        node("omatch.27");
        if (locate(p_use(lin[c_use(*i, "omatch.i", "omatch.27")], "omatch.lin"),
                   p_use(pat, "omatch.pat"), p_use(j, "omatch.j") + 1)) {
          p_loc("omatch.27", "omatch.28");
          node("omatch.28");
          advance = 1;
          def("omatch.advance", "omatch.28");
        } else {
          p_loc("omatch.27", "omatch.29");
          node("omatch.29");
        }
        node("omatch.30");
        break;
      case NCCL:
        p_use(NCCL, "NCCL");
        p_loc("omatch.10", "omatch.31");
        node("omatch.31");
        if ((p_use(lin[c_use(*i, "omatch.i", "omatch.31")], "omatch.lin") !=
             p_use(NEWLINE, "NEWLINE")) &&
            (!locate(
                p_use(lin[c_use(*i, "omatch.i", "omatch.31")], "omatch.lin"),
                p_use(pat, "omatch.pat"), p_use(j, "omatch.j") + 1))) {
          p_loc("omatch.31", "omatch.32");
          node("omatch.32");
          advance = 1;
          def("omatch.advance", "omatch.32");
        } else {
          p_loc("omatch.31", "omatch.33");
          node("omatch.33");
        }
        node("omatch.34");
        break;
      default:
        p_loc("omatch.10", "omatch.35");
        node("omatch.35");
        Caseerror(c_use(pat[c_use(j, "omatch.j", "omatch.35")], "omatch.pat",
                        "omatch.35"));
      };
    }
  }
  node("omatch.36");
  if ((p_use(advance, "omatch.advance") >= 0)) {
    p_loc("omatch.36", "omatch.37");
    node("omatch.37");
    *i = c_use(*i, "omatch.i", "omatch.37") +
         c_use(advance, "omatch.advance", "omatch.37");
    def("omatch.i", "omatch.37");
    node("omatch.38");
    result = true;
    def("omatch.result", "omatch.38");
  } else {
    p_loc("omatch.36", "omatch.39");
    node("omatch.39");
    result = false;
    def("omatch.result", "omatch.39");
  }
  node("omatch.40");
  return c_use(result, "omatch.result", "omatch.40");
}

patsize(pat, n) char *pat;
int n;
{
  node("patsize.0");
  def("patsize.pat", "patsize.0");
  def("patsize.n", "patsize.0");

  node("patsize.1");
  int size;
  def("patsize.size", "patsize.1");

  node("patsize.2");
  if (!in_pat_set(
          p_use(pat[c_use(n, "patsize.n", "patsize.2")], "patsize.pat"))) {
    p_loc("patsize.2", "patsize.3");
    node("patsize.3");
    // (void)fprintf(stdout, "in patsize: can't happen\n");
    node("patsize.4");
    abort();
  } else {
    p_loc("patsize.2", "patsize.5");
    node("patsize.5");
    switch (p_use(pat[c_use(n, "patsize.n", "patsize.5")], "patsize.pat")) {
    case LITCHAR:
      p_use(LITCHAR, "LITCHAR");
      p_loc("patsize.5", "patsize.6");

      node("patsize.6");
      size = 2;
      def("patsize.size", "patsize.6");
      node("patsize.7");
      break;

    case BOL:
      p_use(BOL, "BOL");
      p_loc("patsize.5", "patsize.8");
      node("patsize.8");
    case EOL:
      p_use(EOL, "EOL");
      p_loc("patsize.5", "patsize.9");
      node("patsize.9");
    case ANY:
      p_use(ANY, "ANY");
      p_loc("patsize.5", "patsize.10");
      node("patsize.10");
      size = 1;
      def("patsize.size", "patsize.10");
      node("patsize.11");
      break;
    case CCL:
      p_use(CCL, "CCL");
      p_loc("patsize.5", "patsize.12");
      node("patsize.12");
    case NCCL:
      p_use(NCCL, "NCCL");
      p_loc("patsize.5", "patsize.13");

      node("patsize.13");
      size = c_use(pat[c_use(n, "patsize.n", "patsize.13") + 1], "patsize.pat",
                   "patsize.13") +
             2;
      def("patsize.size", "patsize.13");
      node("patsize.14");
      break;
    case CLOSURE:
      p_use(CLOSURE, "CLOSURE");
      node("patsize.15");
      size = c_use(CLOSIZE, "CLOSURE", "patsize.15");
      def("patsize.size", "patsize.15");
      node("patsize.16");
      break;
    default:
      p_loc("patsize.5", "patsize.17");
      node("patsize.17");
      Caseerror(c_use(pat[c_use(n, "patsize.n", "patsize.17")], "patsize.pat",
                      "patsize.17"));
    }
  }
  node("patsize.18");
  return c_use(size, "patsize.size", "patsize.18");
}

int amatch(lin, offset, pat, j) char *lin;
int offset;
char *pat;
int j;
{
  node("amatch.0");
  def("amatch.lin", "amatch.0");
  def("amatch.offset", "amatch.0");
  def("amatch.pat", "amatch.0");
  def("amatch.j", "amatch.0");

  node("amatch.1");
  int i;
  def("amatch.i", "amatch.1");
  node("amatch.2");
  int k;
  def("amatch.k", "amatch.2");

  node("amatch.3");
  bool result;
  def("amatch.result", "amatch.3");

  node("amatch.4");
  bool done;
  def("amatch.done", "amatch.4");

  node("amatch.5");
  done = false;
  def("amatch.done", "amatch.5");

  node("amatch.6");
  while ((!p_use(done, "amatch.done")) &&
         (p_use(pat[c_use(j, "amatch.j", "amatch.6")], "amatch.pat") !=
          p_use(ENDSTR, "ENDSTR"))) {
    p_loc("amatch.6", "amatch.7");
    node("amatch.7");
    if ((p_use(pat[c_use(j, "amatch.j", "amatch.7")], "amatch.pat") ==
         p_use(CLOSURE, "CLOSURE"))) {
      p_loc("amatch.7", "amatch.8");
      node("amatch.8");
      j = c_use(j, "amatch.j", "amatch.8") +
          patsize(c_use(pat, "amatch.pat", "amatch.8"),
                  c_use(j, "amatch.j", "amatch.8"));
      def("amatch.j", "amatch.8");

      node("amatch.9");
      i = c_use(offset, "amatch.offset", "amatch.9");
      def("amatch.i", "amatch.9");
      node("amatch.10");
      while (
          (!p_use(done, "amatch.done")) &&
          (lin[c_use(i, "amatch.i", "amatch.10")] != p_use(ENDSTR, "ENDSTR"))) {
        p_loc("amatch.10", "amatch.11");
        node("amatch.11");
        result = omatch(c_use(lin, "amatch.lin", "amatch.11"),
                        c_use(&i, "amatch.i", "amatch.11"),
                        c_use(pat, "amatch.pat", "amatch.11"),
                        c_use(j, "amatch.j", "amatch.11"));
        def("amatch.result", "amatch.11");
        node("amatch.12");
        if (!p_use(result, "amatch.result")) {
          p_loc("amatch.12", "amatch.13");
          node("amatch.13");
          done = true;
          def("amatch.done", "amatch.13");
        } else {
          p_loc("amatch.12", "amatch.28");
          node("amatch.28");
        }
        node("amatch.10");
      }
      p_loc("amatch.10", "amatch.14");
      node("amatch.14");
      done = false;
      def("amatch.done", "amatch.14");
      node("amatch.15");
      while ((!p_use(done, "amatch.done")) &&
             (p_use(i, "amatch.i") >= p_use(offset, "amatch.offset"))) {
        p_loc("amatch.15", "amatch.16");
        node("amatch.16");
        k = amatch(c_use(lin, "amatch.lin", "amatch.16"),
                   c_use(i, "amatch.i", "amatch.16"),
                   c_use(pat, "amatch.pat", "amatch.16"),
                   c_use(j, "amatch.j", "amatch.16") +
                       patsize(c_use(pat, "amatch.pat", "amatch.16"),
                               c_use(j, "amatch.j", "amatch.16")));
        def("amatch.k", "amatch.16");
        node("amatch.17");
        if ((p_use(k, "amatch.k") >= 0)) {
          p_loc("amatch.17", "amatch.18");
          node("amatch.18");
          done = true;
          def("amatch.done", "amatch.18");
        } else {
          p_loc("amatch.17", "amatch.19");
          node("amatch.19");
          i = c_use(i, "amatch.i", "amatch.19") - 1;
          def("amatch.i", "amatch.19");
        }
        node("amatch.15");
      }
      p_loc("amatch.15", "amatch.20");
      node("amatch.20");
      offset = c_use(k, "amatch.k", "amatch.20");
      def("amatch.offset", "amatch.20");
      node("amatch.21");
      done = true;
    } else {
      p_loc("amatch.7", "amatch.22");
      node("amatch.22");
      result = omatch(c_use(lin, "amatch.lin", "amatch.22"),
                      c_use(&offset, "amatch.offset", "amatch.22"),
                      c_use(pat, "amatch.pat", "amatch.22"),
                      c_use(j, "amatch.j", "amatch.22"));
      def("amatch.result", "amatch.22");
      node("amatch.23");
      if ((!p_use(result, "amatch.result"))) {
        p_loc("amatch.23", "amatch.24");
        node("amatch.24");
        offset = -1;
        def("amatch.offset", "amatch.24");
        node("amatch.25");
        done = true;
        def("amatch.done", "amatch.25");
      } else {
        p_loc("amatch.23", "amatch.26");
        node("amatch.26");
        j = c_use(j, "amatch.j", "amatch.26") +
            patsize(c_use(pat, "amatch.pat", "amatch.26"),
                    c_use(j, "amatch.j", "amatch.26"));
        def("amatch.j", "amatch.6");
      }
    }
    node("amatch.6");
  }
  p_loc("amatch.6", "amatch.27");
  node("amatch.27");
  return c_use(offset, "amatch.offset", "amatch.27");
}

void putsub(lin, s1, s2, sub) char *lin;
int s1, s2;
char *sub;
{
  node("putsub.0");
  def("putsub.lin", "putsub.0");
  def("putsub.s1", "putsub.0");
  def("putsub.s2", "putsub.0");
  def("putsub.sub", "putsub.0");

  node("putsub.1");
  int i;
  def("putsub.i", "putsub.1");

  node("putsub.2");
  int j;
  def("putsub.j", "putsub.2");

  node("putsub.3");
  i = 0;
  def("putsub.i", "putsub.3");

  node("putsub.4");
  while ((p_use(sub[c_use(i, "putsub.i", "putsub.4")], "putsub.sub") !=
          p_use(ENDSTR, "ENDSTR"))) {
    p_loc("putsub.4", "putsub.5");
    node("putsub.5");
    if ((p_use(sub[c_use(i, "putsub.i", "putsub.5")], "putsub.sub") ==
         p_use(DITTO, "DITTO"))) {
      p_loc("putsub.5", "putsub.6");
      node("putsub.6");
      for (j = c_use(s1, "putsub.s1", "putsub.6"), def("putsub.j", "putsub.6");
           p_use(j, "putsub.j") < p_use(s2, "putsub.s2");
           j = c_use(j, "putsub.j", "putsub.6") + 1,
          def("putsub.j", "putsub.6")) {
        p_loc("putsub.6", "putsub.7");
        node("putsub.7");
        // fputc(c_use(lin[c_use(j, "putsub.j", "putsub.7")], "putsub.lin",
        //             "putsub.7"),
        //       stdout);
        node("putsub.6");
      }
      p_loc("putsub.6", "putsub.12");
      node("putsub.12");
    } else {
      p_loc("putsub.5", "putsub.8");
      node("putsub.8");
      // fputc(c_use(sub[c_use(i,"putsub.i","putsub.8")],"putsub.sub","putsub.8"), stdout);
      node("putsub.9");
    }
    node("putsub.10");
    i = c_use(i,"putsub.i","putsub.10") + 1;
    def("putsub.i","putsub.10");
    node("putsub.4");
  }
  p_loc("putsub.4", "putsub.11");
  node("putsub.11");
}

void subline(lin, pat, sub) char *lin;
char *pat;
char *sub;
{
  node("subline.0");
  def("subline.lin","subline.0");
  def("subline.pat","subline.0");
  def("subline.sub","subline.0");


  node("subline.1");
  int i;
  def("subline.i","subline.1");
  node("subline.2");
  int lastm;
  def("subline.lastm","subline.2");

  node("subline.3");
  int m;
  def("subline.m","subline.3");

  node("subline.4");
  lastm = -1;
  def("subline.lastm","subline.4");

  node("subline.5");
  i = 0;
  def("subline.i","subline.5");
  node("subline.6");
  while ((p_use(lin[c_use(i,"subline.i","subline.6")],"subline.lin") != p_use(ENDSTR,"ENDSTR"))) {
    p_loc("subline.6","subline.7");
    node("subline.7");
    m = amatch(c_use(lin,"subline.lin","subline.7"), c_use(i,"subline.i","subline.7"), c_use(pat,"subline.pat","subline.7"), 0);
    def("subline.m","subline.7");
    node("subline.8");
    if ((p_use(m,"subline.m") >= 0) && (p_use(lastm,"subline.lastm") != p_use(m,"subline.m"))) {
      p_loc("subline.8","subline.9");

      node("subline.9");
      putsub(c_use(lin,"subline.lin","subline.9"), c_use(i,"subline.i","subline.9"), c_use(m,"subline.m","subline.9"), c_use(sub,"subline.sub","subline.9"));

      node("subline.10");
      lastm = c_use(m,"subline.m","subline.10");
        def("subline.lastm","subline.10");
    } else {
      p_loc("subline.8","subline.11");
      node("subline.11");
    }
    node("subline.12");
    if ((p_use(m,"subline.m") == -1) || (p_use(m,"subline.m") == p_use(i,"subline.i"))) {
      p_loc("subline.12","subline.13");
      node("subline.13");
      // fputc(c_use(lin[c_use(i,"subline.i","subline.13")],"subline.lin","subline.13"), stdout);

      node("subline.14");
      i = c_use(i,"subline.i","subline.14") + 1;
      def("subline.i","subline.14");
    } else {
      p_loc("subline.12","subline.15");
      node("subline.15");
      i = c_use(m,"subline.m","subline.15");
      def("subline.i","subline.15");
    }
    node("subline.6");
  }
  p_loc("subline.6","subline.16");
  node("subline.16");
}

void change(pat, sub) char *pat, *sub;
{
  node("change.0");
  def("change.pat","change.0");
  def("change.sub","change.0");


  node("change.1");
  string line;
  def("change.line","change.1");


  node("change.2");
  bool result;
  def("change.result","change.2");


  node("change.3");
  result = getline(c_use(line,"change.line","change.3"), c_use(MAXSTR,"MAXSTR","change.3"));
  def("change.result","change.3");


  node("change.4");
  while ((p_use(result,"change.result"))) {
    p_loc("change.4","change.5");
    node("change.5");
    subline(c_use(line,"change.line","change.5"), c_use(pat,"change.pat","change.5"), c_use(sub,"change.sub","change.5"));
    node("change.6");
    result = getline(c_use(line,"change.line","change.6"), c_use(MAXSTR,"MAXSTR","change.6"));
    def("change.result","change.6");

    node("change.4");
  }
  p_loc("change.4","change.7");

  node("change.7");
}

main(argc, argv) int argc;
char *argv[];
{
  node("0");
  def("MAXSTR","0");
  def("MAXPAT","0");
  def("ENDSTR","0");
  def("ESCAPE","0");
  def("CLOSURE","0");
  def("BOL","0");
  def("EOL","0");
  def("ANY","0");
  def("CCL","0");
  def("CCLEND","0");
  def("NEGATE","0");
  def("NCCL","0");
  def("LITCHAR","0");
  def("DITTO","0");
  def("DASH","0");
  def("TAB","0");
  def("NEWLINE","0");
  def(" CLOSIZE","0");

  node("main.0");
  def("main.argc","main.0");
  def("main.argv","main.0");

  node("main.1");
  string pat;
  def("main.pat","main.1");

  node("main.2");
  string sub;
  def("main.sub","main.2");

  node("main.3");
  bool result;
  def("main.result","main.3");

  node("main.4");
  if (p_use(argc,"main.argc") < 2) {
    p_loc("main.4","main.5");
    node("main.5");
    // (void)fprintf(stdout, "usage: change from [to]\n");
    node("main.6");
    exit(1);
  } else {
    p_loc("main.4","main.7");
    node("main.7");
  }

  node("main.8");
  result = getpat(c_use(argv[1],"main.argv","main.8"), c_use(pat,"main.pat","main.8"));
  def("main.result","main.8");

  node("main.9");
  if (!p_use(result,"main.result")) {
    p_loc("main.9","main.10");
    node("main.10");
    // (void)fprintf(stdout, "change: illegal \"from\" pattern\n");
    node("main.11");
    exit(2);
  } else {
    p_loc("main.9","main.12");

    node("main.12");
  }    p_loc("main.9","main.10");


  node("main.13");
  if (p_use(argc,"main.argc") >= 3) {
    p_loc("main.13","main.14");

    node("main.14");
    result = getsub(c_use(argv[2],"main.argv","main.14"), c_use(sub,"main.sub","main.14"));
    def("main.result","main.14");

    node("main.15");
    if (!p_use(result,"main.result")) {
      p_loc("main.15","main.16");
      node("main.16");
      // (void)fprintf(stdout, "change: illegal \"to\" string\n");
      node("main.17");
      exit(3);
    }else{
      p_loc("main.15","main.21");

      node("main.21");
    }
  } else {
    p_loc("main.13","main.18");
    node("main.18");
    sub[0] = '\0';
    def("main.sub","main.18");
  }
  node("main.19");
  change(c_use(pat,"main.pat","main.19"), c_use(sub,"main.sub","main.19"));

  node("main.20");
  // (void)fprintf(stdout, "\n", c_use(n,"Caseerror.n","Caseerror.1"));
  return 0;
}

void Caseerror(n) int n;
{
  node("Caseerror.0");
  def("Caseerror.n", "Caseerror.0");
  node("Caseerror.1");
  // (void)fprintf(stdout, "Missing case limb: line %d\n", c_use(n,"Caseerror.n","Caseerror.1"));
  node("Caseerror.2");
  exit(4);
}
